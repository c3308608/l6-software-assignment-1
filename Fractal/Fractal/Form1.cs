﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml.Serialization;

namespace Fractal
{
    public partial class Form1 : Form
    {
        #region Conversion of original Java

        class HSB
        { // internal class to convert HSB to RGB
            public float rChan, gChan, bChan;
            public HSB()
            {
                rChan = gChan = bChan = 0;
            }
            public void fromHSB(float h, float s, float b)
            {
                float red = b;
                float green = b;
                float blue = b;
                if (s != 0)
                {
                    float max = b;
                    float dif = b * s / 255f;
                    float min = b - dif;

                    float h2 = h * 360f / 255f;

                    if (h2 < 60f)
                    {
                        red = max;
                        green = h2 * dif / 60f + min;
                        blue = min;
                    }
                    else if (h2 < 120f)
                    {
                        red = -(h2 - 120f) * dif / 60f + min;
                        green = max;
                        blue = min;
                    }
                    else if (h2 < 180f)
                    {
                        red = min;
                        green = max;
                        blue = (h2 - 120f) * dif / 60f + min;
                    }
                    else if (h2 < 240f)
                    {
                        red = min;
                        green = -(h2 - 240f) * dif / 60f + min;
                        blue = max;
                    }
                    else if (h2 < 300f)
                    {
                        red = (h2 - 240f) * dif / 60f + min;
                        green = min;
                        blue = max;
                    }
                    else if (h2 <= 360f)
                    {
                        red = max;
                        green = min;
                        blue = -(h2 - 360f) * dif / 60 + min;
                    }
                    else
                    {
                        red = 0;
                        green = 0;
                        blue = 0;
                    }
                }

                rChan = (float)Math.Round(Math.Min(Math.Max(red, 0f), 255));
                gChan = (float)Math.Round(Math.Min(Math.Max(green, 0), 255));
                bChan = (float)Math.Round(Math.Min(Math.Max(blue, 0), 255));

            }
        }

        private const int MAX = 256;      // max iterations
        private const double SX = -2.025; // start value real
        private const double SY = -1.125; // start value imaginary
        private const double EX = 0.6;    // end value real
        private const double EY = 1.125;  // end value imaginary
        private static int x1, y1, xs, ys, xe, ye;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static bool action, rectangle, finished, cycle, paused; // tc added cycle and paused for colour cycling functionality
        private static float xy;
        private Bitmap picture; // tc original java -- private Image picture;
        private Graphics g1;
        private Cursor c1, c2;
        private HSB HSBcol = new HSB();
        private FractalState state = new FractalState(); // tc added for save state, undo and redo functionality
        private List<FractalState> stateList = new List<FractalState>(); // tc added for save state, undo and redo functionality
        private List<int> stateHistory = new List<int>(); // tc added for save state, undo and redo functionality
        private int pointer; // tc added for save state, undo and redo functionality
        private string scheme; // tc added for colour scheme changing functionality

        public Form1()
        {
            InitializeComponent();

            // tc added methods init() and start() to constructor so they are called when the form loads
            init();
            start();
        }

        public void init() // all instances will be prepared
        {
            // tc not needed -- HSBcol = new HSB();
            // tc not needed -- setSize(640, 480);
            finished = false;
            // tc not needed -- addMouseListener(this);
            // tc not needed -- addMouseMotionListener(this);
            c1 = Cursors.WaitCursor; // tc original java -- c1 = new Cursor(Cursor.WAIT_CURSOR);
            c2 = Cursors.Cross; // tc original java -- c2 = new Cursor(Cursor.CROSSHAIR_CURSOR);
            x1 = pictureBox1.Width; // tc original java -- x1 = getSize().width;
            y1 = pictureBox1.Height; // tc original java -- y1 = getSize().height;
            xy = (float)x1 / (float)y1;
            picture = new Bitmap(x1, y1);
            g1 = Graphics.FromImage(picture); // tc original java -- g1 = picture.getGraphics();
            scheme = "Default"; // tc added for colour scheme changing functionality
            defaultRadioButton.Checked = true; // tc added for colour scheme changing functionality
            trackBar1.Value = 255; // tc added for changing alpha value
            finished = true;
        }

        public void destroy() // delete all instances 
        {
            if (finished)
            {
                // tc not needed -- removeMouseListener(this);
                // tc not needed -- removeMouseMotionListener(this);
                picture = null;
                g1 = null;
                c1 = null;
                c2 = null;
                stateList.Clear(); // tc added for reset functionality
                stateHistory.Clear(); // tc added for reset functionality
                // tc original java -- System.gc(); // garbage collection
                // tc added
                // System.GC.Collect() -- it is not recommended to force garbage collection under normal circumstances
            }
        }

        public void start()
        {
            action = false;
            rectangle = false;
            cycle = false; // tc added for colour cycling functionality
            initvalues();
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            createUndoRedo(); // tc added to enable undo/redo functionality
            mandelbrot();
        }

        /// <summary>
        /// This method is not needed in C#.
        /// </summary>
        //public void stop()
        //{
        //}

        /// <summary>
        /// This method is not needed in C#.
        /// </summary>
        //public void paint(Graphics g)
        //{
        //    update(g);
        //}

        /// <summary>
        /// The logic in this method has been moved into the method pictureBox1_Paint
        /// </summary>
        //public void update(Graphics g)
        //{
        //    g.drawImage(picture, 0, 0, this);
        //    if (rectangle)
        //    {
        //        g.setColor(Color.white);
        //        if (xs < xe)
        //        {
        //            if (ys < ye) g.drawRect(xs, ys, (xe - xs), (ye - ys));
        //            else g.drawRect(xs, ye, (xe - xs), (ys - ye));
        //        }
        //        else
        //        {
        //            if (ys < ye) g.drawRect(xe, ys, (xs - xe), (ye - ys));
        //            else g.drawRect(xe, ye, (xs - xe), (ys - ye));
        //        }
        //    }
        //}

        /// <summary>
        /// Method to paint the fractal onto the pictureBox to make it visible on the form. 
        /// Includes the logic from the Java update method.
        /// </summary>
        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Pen whitePen = new Pen(Color.White);

            e.Graphics.DrawImageUnscaled(picture, 0, 0);

            if (rectangle)
            {
                // tc not needed -- g.setColor(Color.white);
                if (xs < xe)
                {
                    // tc original java -- if (ys < ye) g.drawRect(xs, ys, (xe - xs), (ye - ys));
                    if (ys < ye)
                    {
                        e.Graphics.DrawRectangle(whitePen, xs, ys, (xe - xs), (ye - ys));
                    }
                    // tc original java -- else g.drawRect(xs, ye, (xe - xs), (ys - ye));
                    else
                    {
                        e.Graphics.DrawRectangle(whitePen, xs, ye, (xe - xs), (ys - ye));
                    }
                }
                else
                {
                    // tc original java -- if (ys < ye) g.drawRect(xe, ys, (xs - xe), (ye - ys));
                    if (ys < ye)
                    {
                        e.Graphics.DrawRectangle(whitePen, xe, ys, (xs - xe), (ye - ys));
                    }
                    // tc original java -- else g.drawRect(xe, ye, (xs - xe), (ys - ye));
                    else
                    {
                        e.Graphics.DrawRectangle(whitePen, xe, ye, (xs - xe), (ys - ye));
                    }
                }
            }
        }

        private void mandelbrot() // calculate all points
        {
            int x, y;
            float h, b, alt = 0.0f;

            Pen pen;
            Color color;

            action = false;
            pictureBox1.Cursor = c1; // tc original java -- setCursor(c1);
            statusLabel.Text = "Mandelbrot-Set will be produced - please wait..."; // tc original java -- showStatus("Mandelbrot-Set will be produced - please wait...");

            for (x = 0; x < x1; x += 2)
                for (y = 0; y < y1; y++)
                {

                    h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y); // color value

                    if (h != alt)
                    {
                        b = 1.0f - h * h; // brightness

                        // tc original java -- g1.setColor(Color.getHSBColor(h, 0.8f, b));

                        alt = h;

                        HSBcol.fromHSB(h * 255, 0.8f * 255, b * 255);
                    }

                    // tc added 
                    // sets the colour to use for the pen based on the currently selected colour scheme
                    color = getColour(scheme);

                    pen = new Pen(color);

                    g1.DrawLine(pen, x, y, x + 1, y); // tc original java -- g1.drawLine(x, y, x + 1, y);
                }

            statusLabel.Text = "Mandelbrot-Set ready - please select zoom area with pressed mouse."; //tc original java -- showStatus("Mandelbrot-Set ready - please select zoom area with pressed mouse.");
            pictureBox1.Cursor = c2; // tc original java -- setCursor(c2);
            action = true;
        }

        private float pointcolour(double xwert, double ywert) // color value from 0.0 to 1.0 by iterations
        {
            double r = 0.0, i = 0.0, m = 0.0;
            int j = 0;

            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }
            return (float)j / (float)MAX;
        }

        private void initvalues() // reset start values
        {
            xstart = SX;
            ystart = SY;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;
        }

        /// <summary>
        /// Equivalent of the Java mousePressed method.
        /// </summary>
        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            // tc not needed -- e.consume();
            if (action)
            {
                xs = e.X; // tc original java -- xs = e.getX();
                ys = e.Y; // tc original java -- ys = e.getY();
            }
        }

        /// <summary>
        /// Equivalent of the Java mouseReleased method.
        /// </summary>
        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {

            int z, w;

            //tc not needed -- e.consume();
            if (action)
            {
                xe = e.X; // tc original java -- xe = e.getX();
                ye = e.Y; // tc original java -- ye = e.getY();


                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2)) initvalues();
                else
                {
                    if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                    else xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
                createUndoRedo(); // tc added to enable undo/redo functionality
                mandelbrot();
                rectangle = false;
                Refresh(); // tc original java -- repaint();
            }
        }

        /// <summary>
        /// C# equivalent of the Java mouseEntered method.
        /// </summary>
        private void Form1_MouseEnter(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// C# equivalent of the Java mouseExited method.
        /// </summary>
        private void Form1_MouseLeave(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// C# equivalent of the Java mouseClicked method.
        /// </summary>
        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {

        }

        /// <summary>
        /// In the original Java this code is in the mouseDragged method. There does not seem
        /// to be an equivalent in C# so I've put it in the MouseMove method.
        /// </summary>
        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            // tc not needed -- e.consume();
            if (e.Button == MouseButtons.Left || e.Button == MouseButtons.Right) // tc original java -- if (action)
            {
                xe = e.X;
                ye = e.Y;
                rectangle = true;
                Refresh(); // tc original java -- repaint();
            }
        }

        public String getAppletInfo()
        {
            return "fractal.class - Mandelbrot Set a Java Applet by Eckhard Roessel 2000-2001";
        }

        #endregion

        #region Menu item click methods

        /// <summary>
        /// Displays the save file dialog when the Save Image to File menu item is clicked.
        /// </summary>
        private void saveImageToFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Bitmap (*.bmp)|*.bmp|PNG (*.png)|*.png|JPEG (*.jpg, *.jpeg)|*.jpg;*.jpeg";
            saveFileDialog1.Title = "Save Fractal Image As...";

            saveFileDialog1.ShowDialog();
        }

        /// <summary>
        /// Gives the user the option to save the current state of the fractal when the Save Current State
        /// menu item is clicked.
        /// </summary>
        private void saveCurrentStateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Save the current state?", "Save Current State",
                                                   MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

            if (result == DialogResult.OK)
            {
                writeConfig();
            }
        }

        /// <summary>
        /// Reloads the previously saved fractal state when the Reload menu item is clicked.
        /// </summary>
        private void reloadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Reload the saved state?", "Reload",
                                                   MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

            if (result == DialogResult.OK)
            {
                if (File.Exists("config.xml"))
                {
                    bool loadSuccess = loadConfig();

                    if (loadSuccess == true)
                    {
                        setStateVariables();
                        createUndoRedo();
                        mandelbrot();
                        Refresh();
                    }
                }
                else
                {
                    MessageBox.Show("There is nothing to reload", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        /// <summary>
        /// Resets the fractal to its original default state when the Reset menu item is clicked.
        /// </summary>
        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Reset to the default state?", "Reset",
                                                  MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

            if (result == DialogResult.OK)
            {
                destroy();
                init();
                start();
                Refresh();
            }
        }

        /// <summary>
        /// Closes the application when the Exit menu item is clicked.
        /// </summary>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Copies the currently displayed fractal image to the clipboard when the Copy menu item is clicked.
        /// </summary>
        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetImage(picture);
        }

        /// <summary>
        /// Displays information about the application when the About menu item is clicked.
        /// </summary>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Mandelbrot Fractal in C#, Tom Chambers, 2014." + "\r\n\r\nConverted from " + getAppletInfo(),
                            "About", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Starts the timer when the Start Colour Cycling menu item is clicked.
        /// </summary>
        private void startColourCyclingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (cycle == false || paused == true)
            {
                timer1.Start();
                cycle = true;
                paused = false;
            }
        }

        /// <summary>
        /// Stops the timer when the Pause Colour Cycling menu item is clicked.
        /// </summary>
        private void pauseColourCyclingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (cycle == true)
            {
                timer1.Stop();
                paused = true;
                cycle = false;
            }
        }

        /// <summary>
        /// Stops the timer and redraws the fractal when the Stop Colour Cycling menu item is clicked.
        /// </summary>>
        private void stopColourCyclingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (cycle == true || paused == true)
            {
                cycle = false;
                paused = false;
                timer1.Stop();
                mandelbrot();
                Refresh();
            }
        }

        #endregion

        #region Load save and close methods

        /// <summary>
        /// Saves the fractal image to a file with the chosen extension.
        /// </summary>
        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            string name = saveFileDialog1.FileName;

            string extension = Path.GetExtension(saveFileDialog1.FileName);

            switch (extension.ToLower())
            {
                case ".png":
                    picture.Save(name, ImageFormat.Png);
                    break;
                case ".jpg":
                    picture.Save(name, ImageFormat.Jpeg);
                    break;
                case ".bmp":
                    picture.Save(name, ImageFormat.Bmp);
                    break;
                default:
                    MessageBox.Show("Please enter .bmp, .png, .jpg or .jpeg as the file extension");
                    e.Cancel = true;
                    break;
            }
        }

        /// <summary>
        /// Saves the current state of the fractal by serializing the FractalState class to the xml config file.
        /// </summary>
        private void writeConfig()
        {
            using (StreamWriter sw = new StreamWriter("config.xml"))
            {
                XmlSerializer ser = new XmlSerializer(typeof(FractalState));
                ser.Serialize(sw, state);
            }
        }

        /// <summary>
        /// Deserializes the FractalState class to allow the previously saved fractal state to be reloaded.
        /// </summary>
        private bool loadConfig()
        {
            XmlSerializer ser = new XmlSerializer(typeof(FractalState));
            try
            {
                using (FileStream fs = File.OpenRead("config.xml"))
                {
                    state = (FractalState)ser.Deserialize(fs);

                    return true;
                }
            }
            catch (InvalidOperationException e)
            {
                MessageBox.Show(e.Message, "Reload Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return false;
            }
        }

        /// <summary>
        /// Gives the user the option to save the current fractal state before the form closes.
        /// </summary>
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            // only ask the user if they want to save the current state if the fractal is not in its original state
            if (stateList.Count > 1 && pointer > 0)
            {
                DialogResult result = MessageBox.Show("Would you like to save the current state of the fractal, so it can be reloaded later?",
                                                      "Save state?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    writeConfig();
                    destroy();
                }
                else if (result == DialogResult.No)
                {
                    destroy();
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        #endregion

        #region Colour cycling methods

        /// <summary>
        /// Changes the palette at each timer tick event during colour cycling.
        /// </summary>
        private void timer1_Tick(object sender, EventArgs e)
        {
            changePalette();
        }

        /// <summary>
        /// Changes the palette when the user clicks the Change Palette button.
        /// </summary>
        private void changePaletteButton_Click(object sender, EventArgs e)
        {
            changePalette();
        }

        /// <summary>
        /// Updates the fractal with a new colour palette.
        /// </summary>
        private void changePalette()
        {
            Bitmap newImage = colorCycle(picture);
            g1 = Graphics.FromImage(picture);

            g1.DrawImage(newImage, 0, 0);
            pictureBox1.Image = picture;
        }

        /// <summary>
        /// Returns a new bitmap with a new colour palette.
        /// </summary>
        private Bitmap colorCycle(Bitmap original)
        {
            MemoryStream stream = new MemoryStream();
            original.Save(stream, ImageFormat.Gif);
            Bitmap newBitmap = new Bitmap(stream);

            ColorPalette newPalette = newBitmap.Palette;

            for (int i = 0; i < newPalette.Entries.Length - 5; i++)
            {
                Color color = newPalette.Entries[i];
                newPalette.Entries[i] = newPalette.Entries[i + 1];
                newPalette.Entries[i + 1] = color;
            }

            newBitmap.Palette = newPalette;
            return newBitmap;
        }

        /// <summary>
        /// Changes the speed of the colour cycling when the user changes the value in the NumericUpDown box.
        /// </summary>>
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            timer1.Interval = ((int)numericUpDown1.Value * 10) * (int)numericUpDown1.Value;
        }

        #endregion

        #region Colour switch methods

        /// <summary>
        /// Detects which colour scheme the user has selected and updates the fractal with the new scheme.
        /// </summary>
        private void changeColourSchemeButton_Click(object sender, EventArgs e)
        {

            if (redRadioButton.Checked == true)
            {
                if (!scheme.Equals("Red"))
                {
                    switchPalette("Red");
                }
            }
            else if (yellowRadioButton.Checked == true)
            {
                if (!scheme.Equals("Yellow"))
                {
                    switchPalette("Yellow");
                }
            }
            else if (greenRadioButton.Checked == true)
            {
                if (!scheme.Equals("Green"))
                {
                    switchPalette("Green");
                }
            }
            else if (blueRadioButton.Checked == true)
            {
                if (!scheme.Equals("Blue"))
                {
                    switchPalette("Blue");
                }
            }
            else if (purpleRadioButton.Checked == true)
            {
                if (!scheme.Equals("Purple"))
                {
                    switchPalette("Purple");
                }
            }
            else if (pinkRadioButton.Checked == true)
            {
                if (!scheme.Equals("Pink"))
                {
                    switchPalette("Pink");
                }
            }
            else if (cyanRadioButton.Checked == true)
            {
                if (!scheme.Equals("Cyan"))
                {
                    switchPalette("Cyan");
                }
            }
            else if (limeRadioButton.Checked == true)
            {
                if (!scheme.Equals("Lime"))
                {
                    switchPalette("Lime");
                }
            }
            else if (khakiRadioButton.Checked == true)
            {
                if (!scheme.Equals("Khaki"))
                {
                    switchPalette("Khaki");
                }
            }
            else if (silverRadioButton.Checked == true)
            {
                if (!scheme.Equals("Silver"))
                {
                    switchPalette("Silver");
                }
            }
            else if (greyRadioButton.Checked == true)
            {
                if (!scheme.Equals("Grey"))
                {
                    switchPalette("Grey");
                }
            }
            else if (whiteRadioButton.Checked == true)
            {
                if (!scheme.Equals("White"))
                {
                    switchPalette("White");
                }
            }
            else
            {
                if (!scheme.Equals("Default"))
                {
                    switchPalette("Default");
                }
            }
        }

        /// <summary>
        /// Changes the colour scheme.
        /// </summary>
        private void switchPalette(string newScheme)
        {
            scheme = newScheme;
            createUndoRedo();
            mandelbrot();
            Refresh();
        }

        /// <summary>
        /// Gets the colour for the pen based on the colour scheme selected by the user.
        /// </summary>
        private Color getColour(string scheme)
        {
            Color colour;

            int alpha = trackBar1.Value; // Sets the alpha value to the value the user has selected

            switch (scheme)
            {
                case "Red":
                    colour = Color.FromArgb(alpha, (int)HSBcol.rChan, (int)HSBcol.gChan, (int)HSBcol.gChan); // red scheme
                    break;
                case "Yellow":
                    colour = Color.FromArgb(alpha, (int)HSBcol.rChan, (int)HSBcol.rChan, (int)HSBcol.gChan); // yellow scheme
                    break;
                case "Green":
                    colour = Color.FromArgb(alpha, (int)HSBcol.bChan, (int)HSBcol.gChan, (int)HSBcol.bChan); // green scheme
                    break;
                case "Blue":
                    colour = Color.FromArgb(alpha, (int)HSBcol.gChan, (int)HSBcol.bChan, (int)HSBcol.rChan); // blue scheme
                    break;
                case "Purple":
                    colour = Color.FromArgb(alpha, (int)HSBcol.gChan, (int)HSBcol.bChan, (int)HSBcol.gChan); // purple scheme
                    break;
                case "Pink":
                    colour = Color.FromArgb(alpha, (int)HSBcol.rChan, (int)HSBcol.bChan, (int)HSBcol.gChan); // pink scheme
                    break;
                case "Cyan":
                    colour = Color.FromArgb(alpha, (int)HSBcol.gChan, (int)HSBcol.rChan, (int)HSBcol.rChan); // cyan scheme
                    break;
                case "Lime":
                    colour = Color.FromArgb(alpha, (int)HSBcol.gChan, (int)HSBcol.rChan, (int)HSBcol.gChan); // lime scheme
                    break;
                case "Khaki":
                    colour = Color.FromArgb(alpha, (int)HSBcol.gChan, (int)HSBcol.gChan, (int)HSBcol.bChan); // khaki scheme
                    break;
                case "Silver":
                    colour = Color.FromArgb(alpha, (int)HSBcol.bChan, (int)HSBcol.gChan, (int)HSBcol.gChan); // silver scheme
                    break;
                case "Grey":
                    colour = Color.FromArgb(alpha, (int)HSBcol.gChan, (int)HSBcol.gChan, (int)HSBcol.gChan); // grey scheme
                    break;
                case "White":
                    colour = Color.FromArgb(alpha, (int)HSBcol.rChan, (int)HSBcol.rChan, (int)HSBcol.rChan); // white scheme
                    break;
                default:
                    colour = Color.FromArgb(alpha, (int)HSBcol.rChan, (int)HSBcol.gChan, (int)HSBcol.bChan); // default
                    break;
            }

            return colour;
        }

        #endregion

        #region Undo/redo

        /// <summary>
        /// Creates a new state object to enable undo, redo and save state operations.
        /// </summary>
        private void createUndoRedo()
        {
            state = new FractalState();

            state.xs = xs;
            state.ys = ys;
            state.xe = xe;
            state.ye = ye;

            state.xstart = xstart;
            state.ystart = ystart;
            state.xende = xende;
            state.yende = yende;
            state.xzoom = xzoom;
            state.yzoom = yzoom;

            state.scheme = scheme;

            state.defaultRadioButton = defaultRadioButton.Checked;
            state.redRadioButton = redRadioButton.Checked;
            state.yellowRadioButton = yellowRadioButton.Checked;
            state.greenRadioButton = greenRadioButton.Checked;
            state.blueRadioButton = blueRadioButton.Checked;
            state.purpleRadioButton = purpleRadioButton.Checked;
            state.pinkRadioButton = pinkRadioButton.Checked;
            state.cyanRadioButton = cyanRadioButton.Checked;
            state.limeRadioButton = limeRadioButton.Checked;
            state.khakiRadioButton = khakiRadioButton.Checked;
            state.silverRadioButton = silverRadioButton.Checked;
            state.greyRadioButton = greyRadioButton.Checked;
            state.whiteRadioButton = whiteRadioButton.Checked;

            stateList.Add(state); // add the current state to the list of states

            stateHistory.Add(stateList.Count() - 1); // add the state to the history so it can be returned to later

            pointer = stateHistory.Count() - 1; // set the pointer equal to the current state
        }

        /// <summary>
        /// Returns the fractal to the previous state.
        /// </summary>
        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // only perform the undo operation if the fractal is not in its original state
            if (stateList.Count() > 1 && pointer > 0)
            {
                // add to the state history so this state can be returned to directly after a new state has been created
                pointer--;
                stateHistory.Add(stateHistory.ElementAt(pointer));

                // return the fractal to the previous state in the state history
                state = stateList.ElementAt(stateHistory.ElementAt(pointer));

                setStateVariables();
                mandelbrot();
                Refresh();
            }
        }

        /// <summary>
        /// Reverses the previous undo operation.
        /// </summary>
        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // only perform the redo operation if the fractal is not in its most recently created state
            if (!(stateHistory.ElementAt(pointer) == stateList.Count() - 1))
            {
                // remove the last undo from the state history
                stateHistory.RemoveAt(stateHistory.Count() - 1);

                // return the fractal to the state it was in before the undo that was removed from the state history
                pointer++;
                state = stateList.ElementAt(stateHistory.ElementAt(pointer));

                setStateVariables();
                mandelbrot();
                Refresh();
            }
        }

        private void setStateVariables()
        {
            xs = state.xs;
            ys = state.ys;
            xe = state.xe;
            ye = state.ye;

            ystart = state.ystart;
            xstart = state.xstart;
            xende = state.xende;
            yende = state.yende;
            xzoom = state.xzoom;
            yzoom = state.yzoom;

            scheme = state.scheme;

            defaultRadioButton.Checked = state.defaultRadioButton;
            redRadioButton.Checked = state.redRadioButton;
            yellowRadioButton.Checked = state.yellowRadioButton;
            greenRadioButton.Checked = state.greenRadioButton;
            blueRadioButton.Checked = state.blueRadioButton;
            purpleRadioButton.Checked = state.purpleRadioButton;
            pinkRadioButton.Checked = state.pinkRadioButton;
            cyanRadioButton.Checked = state.cyanRadioButton;
            limeRadioButton.Checked = state.limeRadioButton;
            khakiRadioButton.Checked = state.khakiRadioButton;
            silverRadioButton.Checked = state.silverRadioButton;
            greyRadioButton.Checked = state.greyRadioButton;
            whiteRadioButton.Checked = state.whiteRadioButton;
        }

        #endregion
    }
}