﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fractal
{
    public class FractalState
    {
        public int xs { get; set; }
        public int ys { get; set; }
        public int xe { get; set; }
        public int ye { get; set; }

        public double xstart { get; set; }
        public double ystart { get; set; }
        public double xende  { get; set; }
        public double yende  { get; set; }
        public double xzoom  { get; set; }
        public double yzoom  { get; set; }

        public string scheme { get; set; }

        public bool defaultRadioButton { get; set; }
        public bool redRadioButton     { get; set; }
        public bool yellowRadioButton  { get; set; }
        public bool greenRadioButton   { get; set; }
        public bool blueRadioButton    { get; set; }
        public bool purpleRadioButton  { get; set; }
        public bool pinkRadioButton    { get; set; }
        public bool cyanRadioButton    { get; set; }
        public bool limeRadioButton    { get; set; }
        public bool khakiRadioButton   { get; set; }
        public bool silverRadioButton  { get; set; }
        public bool greyRadioButton    { get; set; }
        public bool whiteRadioButton   { get; set; }
    }
}
